import Navbar from "./Navbar";
import Header from "./Header";
import { PoliciesContextProvider, Filters, Search, Table, Reset, } from "./Policies";

import "./index.css";
import { memo } from "react";
const App = () => (
  <div>
    <Navbar />
    <div className="w-full p-8">
      <Header />
      <PoliciesContextProvider>
        <div className="flex flex-row items-end">
          <Search />
          <div className="flex flex-row">
            <Filters />
            <Reset />
          </div>
        </div>
        <Table />
      </PoliciesContextProvider>
    </div>
  </div>
);


export default memo(App);
