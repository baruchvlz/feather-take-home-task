import { Policy } from "./types";

// maybe move to shared constants file?
const API_URL = "http://localhost:4000";
const POLICIES_ENDPOINT = "/policies";

export async function loadPoliciesData(): Promise<Policy[]> {
  try {
    const response = await fetch(`${API_URL}${POLICIES_ENDPOINT}`, {
      mode: "cors",
      method: "GET",
    });
    const json = await response.json();
    return json;
  } catch (error) {
    console.error("--- ERROR FETCHING POLICIES ---");
    console.error(error);
    console.error("-------------- END ------------");
    return [];
  }
}