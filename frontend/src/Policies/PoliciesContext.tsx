import { createContext, memo, PropsWithChildren, useCallback, useContext, useEffect, useState } from "react";
import { loadPoliciesData } from "./loadPoliciesData";
import { Policy, HasData, FilterTypes } from "./types";

type Filters = Record<keyof(FilterTypes), string>;

interface PoliciesContext extends HasData<Policy[]> {
  search: { value: string, onSearch: (value: string) => void }
  filters: { current: Filters, applyFilters(name: string, value: string): void },
  reset: () => void;
};

const defaultFilters: Filters = {
  status: "ALL", insuranceType: "ALL", provider: "ALL",
};

const defaultContext: PoliciesContext = {
  search: { value: "", onSearch: console.log },
  filters: { current: defaultFilters, applyFilters: console.log },
  data: [],
  reset: console.error
};

const ctx = createContext<PoliciesContext>(defaultContext);

export function usePoliciesContext(): PoliciesContext {
  const context = useContext<PoliciesContext>(ctx);

  if (!context) {
    throw new Error("usePoliciesContext must be used inside a PoliciesContext provider");
  }

  return { ...context };
}

function PoliciesContextProvider({ children }: PropsWithChildren<{}>) {
  const [policies, setPolicies] = useState<Policy[]>([]);
  const [searchValue, setSearchValue] = useState<string>("");
  const [filters, setFilters] = useState<Filters>(defaultFilters);

  useEffect(() => {
    loadPoliciesData().then(setPolicies);
  }, []);

  const onSearch = useCallback((value: string) => {
    setSearchValue(value);
  }, [setSearchValue]);

  const applyFilters = useCallback((name: string, value: string) => {
    setFilters((currentFilters) => ({ ...currentFilters, [name]: value }));
  }, [setFilters]);

  const resetSearchAndFilter = useCallback(() => {
    setSearchValue("");
    setFilters(defaultFilters);
  }, [setSearchValue, setFilters]);

  return (
    <ctx.Provider value={{
      search: { value: searchValue, onSearch },
      filters: { current: filters, applyFilters },
      data: policies.filter(p => p.status === "ACTIVE" || p.status === "PENDING"),
      reset: resetSearchAndFilter,
    }}>
      {children}
    </ctx.Provider>
  );
}

export default memo(PoliciesContextProvider);