import { cleanup, fireEvent, render, screen, wait, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import Filters from "../Filters";
import PoliciesContextProvider from "../PoliciesContext";
import Reset from "../Reset";
import Search from "../Search";
import Table from "../Table";

/**
 * @NOTE
 * Unfortuantely this mock is not working properly and the tests
 * only run with the docker image spun up (that's why the expected results on the test are so high)
 * i didn't have time to debug this
 */
jest.mock("loadPoliciesData");

enum TestId {
  Search = "search",
  Table = "table",
  TableHeader = "table-header",
  TableBody = "table-body",
  TableBodyResult = "test-body__result",
  Filters = "filters",
  FilterSelect = "filter-select",
  Reset = "reset",
}

const Component = () => (
  <PoliciesContextProvider>
    <div className="flex flex-row items-end justify-between">
      <Search />
      <div className="flex flex-row">
        <Filters />
        <Reset />
      </div>
    </div>
    <Table />
  </PoliciesContextProvider>
);

describe("PoliciesContextProvider", () => {
  beforeEach(async () => { render(<Component />); });

  test("search", async () => {
    const testElement = screen.getByTestId(TestId.Search);
    const inputEl = testElement.querySelector("input");
    const buttonEl = testElement.querySelector("button");

    await waitFor(async () => expect(await screen.findAllByTestId(TestId.TableBodyResult)));

    if (testElement && inputEl && buttonEl) {
      fireEvent.change(inputEl, { target: { value: "Bi" } });
      fireEvent.click(buttonEl);

      const results = await screen.findAllByTestId(TestId.TableBodyResult);

      expect(results).toHaveLength(6);
    }
  });

  test("reset", async () => {
    const selects = screen.getByTestId(TestId.Filters).querySelectorAll("section");
    const search = screen.getByTestId(TestId.Search);
    const reset = screen.getByTestId(TestId.Reset);

    userEvent.type(search, "test");
    userEvent.selectOptions(selects[0].querySelector("select")!, "PENDING");
    userEvent.selectOptions(selects[1].querySelector("select")!, "HOUSEHOLD");
    userEvent.selectOptions(selects[2].querySelector("select")!, "DAK");
    

    userEvent.click(reset);

    await waitFor(async () => expect(await screen.findAllByTestId(TestId.TableBodyResult)));

    expect(search.querySelector("input")?.value).toBeFalsy();
    selects.forEach(select => {
      expect(select.querySelector("select")?.value).toBe("ALL");
    });
  });

  describe("filters", () => {
    const config: Array<{ name: string, option: string, length: number; }> = [
      { name: "status",  option: "PENDING", length: 24 },
      { name: "insuranceType",  option: "HOUSEHOLD", length: 9 },
      { name: "provider", option: "DAK", length: 6 },
    ];

    config.forEach(({ name, option, length }) => {
      test(name, async () => {
        userEvent.selectOptions(screen.getByTestId(`filter-select__${name}`).querySelector("select")!, option);
        const results = await screen.findAllByTestId(TestId.TableBodyResult);
        expect(results).toHaveLength(length);
      });
    })
  });

  afterAll(cleanup);
});
