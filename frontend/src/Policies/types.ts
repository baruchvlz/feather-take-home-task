export type InsuranceStatus = 'ACTIVE' | 'PENDING' | 'CANCELLED' | 'DROPPED_OUT';
export type InsuranceType = 'LIABILITY' | 'HEALTH' | 'HOUSEHOLD';
export type InsuranceProviders  = 'BARMER' | 'AOK' | 'TK' | 'DAK';
export type Filter = Record<keyof (FilterTypes), string[]>;

export interface Customer {
  dateOfBirth: string;
  firstName: string;
  id: string;
  lastName: string;
}

export interface FilterTypes {
  status: InsuranceStatus;
  insuranceType: InsuranceType,
  provider: InsuranceProviders;
}

export interface Policy extends FilterTypes {
  id: string;
  customer: Customer;

  endDate: string | null;
  startDate: string;
  
}

/// Helpers
export interface HasData<T = any> {
  data: T;
}