import { memo } from "react";
import { usePoliciesContext } from "./PoliciesContext";
import { Filter, FilterTypes } from "./types";

type FilterDropdownProps = { name: string, values: string[], onSelect(value: string): void };

const availableFilters: Filter = {
  status: ['ALL', 'ACTIVE', 'PENDING'],
  insuranceType: ['ALL', 'LIABILITY', 'HEALTH', 'HOUSEHOLD'],
  provider: ['ALL', 'BARMER', 'AOK', 'TK', 'DAK']
}

const upperCase = (s: string) => `${s.charAt(0).toUpperCase()}${s.slice(1)}`;

function FilterDropdown({ name, values, onSelect }: FilterDropdownProps) {
  const { filters: { current: currentFilters } } = usePoliciesContext();

  return (
    <section className="flex flex-col" data-testid={`filter-select__${name}`}>
      <label htmlFor={name}>{upperCase(name)}</label>
      <select
        name={name}
        style={{ marginRight: 12 }}
        value={currentFilters[name as keyof(FilterTypes)]}
        onChange={e => onSelect(e.target.value)}
      >
        {values.map(value => (<option key={value} value={value}>{upperCase(value)}</option>))}
      </select>
    </section>
  )
}

function Filters() {
  const { filters: { applyFilters } } = usePoliciesContext();

  return (
    <div data-testid="filters" className="flex flex-row">
      {
        Object.entries(availableFilters).map(([name, values]) => (
          <FilterDropdown
            key={name} 
            name={name}
            values={values}
            onSelect={(val: string) => {
              applyFilters(name, val);
            }}
          />
        ))
      }
    </div>
  );
}

export default memo(Filters);
