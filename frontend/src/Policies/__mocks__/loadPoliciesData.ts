import { Policy } from "../types";

export default function loadPoliciesData(): Promise<Policy[]> {
  return new Promise(res => res([
    {
      "id": "d2f5aa39-9418-43b4-baf9-13a22375ab48",
      "provider": "BARMER",
      "insuranceType": "HEALTH",
      "status": "PENDING",
      "startDate": "2017-04-26T05:32:06.000Z",
      "endDate": null,
      "customer": {
        "id": "89c49873-6778-49d2-82b7-b74dff178757",
        "firstName": "Cyrillus",
        "lastName": "Biddlecombe",
        "dateOfBirth": "1978-12-03T06:33:17.000Z"
      }
    },
    {
      "id": "9bf86614-e27d-42b6-80b5-7131934da6b3",
      "provider": "BARMER",
      "insuranceType": "LIABILITY",
      "status": "PENDING",
      "startDate": "2015-01-13T04:52:15.000Z",
      "endDate": null,
      "customer": {
        "id": "c794bb43-42b0-4380-8d1a-6c409d9d86e9",
        "firstName": "Brandy",
        "lastName": "Harbour",
        "dateOfBirth": "1985-02-28T12:51:27.000Z"
      }
    },
    {
      "id": "693594f5-af7f-468b-b583-0fe2514d16db",
      "provider": "AOK",
      "insuranceType": "HEALTH",
      "status": "DROPPED_OUT",
      "startDate": "2014-07-14T00:54:34.000Z",
      "endDate": null,
      "customer": {
        "id": "b068ebae-1e21-4719-a5e7-3ea1e58d848f",
        "firstName": "Ailina",
        "lastName": "Harber",
        "dateOfBirth": "1993-01-20T02:51:20.000Z"
      }
    },
    {
      "id": "897625b2-b2ed-4c53-a7b6-0089e6ea75d1",
      "provider": "AOK",
      "insuranceType": "HEALTH",
      "status": "PENDING",
      "startDate": "2020-07-21T19:40:35.000Z",
      "endDate": null,
      "customer": {
        "id": "bbd72227-98a1-4ba4-b41f-c1a254d3b847",
        "firstName": "Aguste",
        "lastName": "Bilsford",
        "dateOfBirth": "1997-04-24T11:26:05.000Z"
      }
    }
  ]));
}