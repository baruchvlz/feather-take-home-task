import { ReactElement, useCallback } from "react";
import { usePoliciesContext } from "./PoliciesContext";

const checkIfString = (v: any) => typeof v === "string";

// even though the searching is done internally via the context
// exposing an `onClick` function might have some benefits
// for example: if we would want to track search words
const Search = ({ onClick }: { onClick?: (value: string) => void }): ReactElement => {
  const { search: { onSearch, value } } = usePoliciesContext();

  const handleOnClick = useCallback(e => {
    e.preventDefault();

    // empty strings are falsy values but valid search inputs
    if (checkIfString(e.target.value)) {
      // using null assertion because typescript doesn't understand the conditional
      onClick?.(e.target.value);
      onSearch(e.target.value);
    }

  }, [onClick, onSearch]);

  return (
    <form style={{ marginRight: 12 }} className="flex items-end" data-testid="search">
      <input
        style={{ marginRight: 4 }}
        placeholder="Search By Name"
        value={value}
        onChange={handleOnClick}  
      />
      {/* <button className="px-2 py-1 border border-transparent rounded-sm text-base text-white bg-gray-600 hover:bg-gray-700" onClick={handleOnClick}>Go</button> */}
    </form>
  );
}

export default Search;