import { memo, ReactElement, useEffect, useMemo, useState } from "react";
import { usePoliciesContext } from "./PoliciesContext";
import { Policy } from "./types";

import Badge from "./Badge";

const lCase = (s: string) => s.toLocaleLowerCase();

const Table = (): ReactElement => {
  const { search, data, filters: { current: currentFilters } } = usePoliciesContext();
  const headers = useMemo(() => ["#", "Name", "Provider", "Type", "Status"], []);
  const [tableData, setTableData] = useState<Policy[]>([]);

  useEffect(() => setTableData(data), [data]);

  useEffect(() => {
    let nextData: Policy[] = [...data].filter(policy => {
      return (
        (currentFilters.insuranceType === "ALL" || currentFilters.insuranceType === policy.insuranceType) &&
        (currentFilters.status === "ALL" || currentFilters.status === policy.status) &&
        (currentFilters.provider === "ALL" || currentFilters.provider === policy.provider)
      );
    });

    if (Boolean(search.value)) {
      nextData = nextData.filter(
        ({ customer: { firstName, lastName } }) =>
          lCase(firstName).startsWith(lCase(search.value)) || lCase(lastName).startsWith(lCase(search.value))
      );
    }

    setTableData(nextData);
  }, [search, currentFilters, data, setTableData]);

  return (
    <div className="flex flex-col" data-testid="table">
      <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="py-2 inline-block min-w-full sm:px-6 lg:px-8">
          <div className="overflow-hidden rounded-lg shadow-sm">
            <table className="min-w-full">
              <thead className="border-b bg-gray-100" data-testid="table-header">
                <tr>
                  {headers.map((header: string) => (
                    <th key={`header-${header}`} scope="col" className="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                      {header}
                    </th>
                  ))}
                </tr>
              </thead>
              <tbody data-testid="table-body">
                {tableData.length > 0 ? tableData.map((policy: Policy, index: number) => (
                  <tr data-testid="test-body__result" className="border-b" key={`policy-${policy.id}`}>
                    <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">{index + 1}</td>
                    <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                      {policy.customer.firstName} {policy.customer.lastName}
                    </td>
                    <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                      {policy.provider}
                    </td>
                    <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                      {policy.insuranceType}
                    </td>
                    <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                      <Badge status={policy.status} />
                    </td>
                  </tr>
                )) : <tr><td>{data.length > 0 ? "No Matches" : "Fetching Data"}</td></tr>}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
}

export default memo(Table);