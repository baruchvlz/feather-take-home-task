import { usePoliciesContext } from "./PoliciesContext";

export default function Reset() {
  const { reset } = usePoliciesContext();

  return (
    <button
      className="px-2 py-1 border border-transparent rounded-sm text-base text-white bg-gray-600 hover:bg-gray-700"
      onClick={reset}
      data-testid="reset"
    >
      Reset
    </button>
  );
}