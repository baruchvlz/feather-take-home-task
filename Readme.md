# Feather Take Home Assessment

## General questions

- How much time did you spend working on the solution?
  - From project setup to task completion around 2-3 hours. I spent another 1 hour or so code reviewing and fixing small things

- What’s the part of the solution you are most proud of?
  - I like how I used the context API in order to have more granular components and tie them all together without causing too many re-render

- If you had more time, what other things you would like to do?
  - figure out a way to share types between backend and frontend
  - less inline styling
  - create a reusable "Button" component to avoid having to repeat styles (sign up, go, and reset buttons would use it)
  - do the backend task as well, i was a bit constrain in time and had already pushed the task too far back so I wanted to focus on one task
  - fix the fetch mock in tests

- Do you have any feedback regarding this coding challenge?  
  - I enjoyed working on this challenge, is definitely different from many others. I particularly like how Feather was able to create 1 challenge that applies for three different positions, that's very smart.
